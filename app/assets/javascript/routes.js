angular.module('Angie').config(function($routeProvider){
  $routeProvider
    .when('/', {
      redirectTo: '/users'
    })
    .when('/users', {
      templateUrl: "assets/templates/users/index.html",
      controller: "UsersIndexController"
    })

    .when('/users/:id', {
      templateUrl: "assets/templates/users/show.html",
      controller: "UsersShowController"
    });

});