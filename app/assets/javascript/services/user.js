angular.module('Angie').factory('User', function($resource){
  return $resource('/users/:id');
});